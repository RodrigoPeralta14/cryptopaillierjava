package crypto;

import java.math.BigInteger;
import java.util.Random;

public class PaillierPublicKey {

    private int bits;
    private BigInteger n;
    private BigInteger nSquared;
    private BigInteger nPlus1;

    public PaillierPublicKey(int bits, BigInteger n) {
        this.bits = bits;
        this.n = n;
        this.nSquared = n.multiply(n);
        this.nPlus1 = n.add(BigInteger.ONE);
    }

    public BigInteger getN() {
        return n;
    }

    public BigInteger getnSquared() {
        return nSquared;
    }

    public BigInteger getnPlus1() {
        return nPlus1;
    }

    public int getBits() {
        return bits;
    }


    public BigInteger encrypt(BigInteger m) {

        BigInteger c = this.n.multiply(m).add(BigInteger.ONE).mod(this.nSquared);
        BigInteger r = this.randomZStar();

        return (c.multiply(r)).mod(this.nSquared);

    }

    public BigInteger randomZStar() {

        BigInteger r;
        do {

            r = new BigInteger(this.bits, new Random());

        } while (r.compareTo(this.n) > 0);

        return r.modPow(this.n, this.nSquared);


    }

    @Override
    public String toString() {

        return this.n.toString();

    }

    public BigInteger getValue() {

        return this.n;

    }
}
