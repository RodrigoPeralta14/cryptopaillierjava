package crypto;

import java.math.BigInteger;

public class PaillierPrivateKey {

    private BigInteger lambda;

    private PaillierPublicKey publicKey;

    private BigInteger x;

    public PaillierPrivateKey(BigInteger lambda, PaillierPublicKey paillierPublicKey) {

        this.lambda = lambda;

        this.publicKey = paillierPublicKey;

        this.x = publicKey.getnPlus1().modPow(lambda, publicKey.getnSquared()).subtract(BigInteger.ONE).divide(publicKey.getN()).modInverse(publicKey.getN());


    }

    public BigInteger getLambda() {
        return lambda;
    }

    public PaillierPublicKey getPublicKey() {
        return publicKey;
    }


    public BigInteger decrypt(BigInteger cipher) {

        return cipher.modPow(this.lambda, this.publicKey.getnSquared()).subtract(BigInteger.ONE).divide(this.publicKey.getN()).multiply(this.x).mod(this.publicKey.getN());

    }

    @Override
    public String toString() {

        return this.lambda.toString();

    }

    public BigInteger getValue() {

        return this.lambda;

    }
}
