package crypto;

import java.math.BigInteger;
import java.util.Random;

//using as reference https://github.com/mhe/jspaillier/blob/master/paillier.js
//should be cryptoanalized

public class PaillierKeyPairGenerator {

    static private BigInteger lcm(BigInteger a, BigInteger b) {
        return a.multiply(b).divide(a.gcd(b));
    }

    public static PaillierKeyPairContainer generate(int modulus, BigInteger n, BigInteger lambda) {

        PaillierPublicKey publicKey = new PaillierPublicKey(modulus, n);
        PaillierPrivateKey privateKey = new PaillierPrivateKey(lambda, publicKey);
        return new PaillierKeyPairContainer(publicKey, privateKey);

    }

    public static PaillierKeyPairContainer generate(int modulus) {

        BigInteger p;
        BigInteger q;
        BigInteger n;

        do {

            do {

                p = new BigInteger(modulus >> 1, 1, new Random());

            } while (!p.isProbablePrime(10));

            do {

                q = new BigInteger(modulus >> 1, 1, new Random());

            } while (!q.isProbablePrime(10));

            n = p.multiply(q);

        } while (!(n.testBit(modulus - 1)) || (p.compareTo(q) == 0));

        PaillierPublicKey publicKey = new PaillierPublicKey(modulus, n);
        BigInteger lambda = lcm(p.subtract(BigInteger.ONE), q.subtract(BigInteger.ONE));
        PaillierPrivateKey privateKey = new PaillierPrivateKey(lambda, publicKey);
        return new PaillierKeyPairContainer(publicKey, privateKey);

    }


}
