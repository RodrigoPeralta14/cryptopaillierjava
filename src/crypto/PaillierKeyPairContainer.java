package crypto;

import java.math.BigInteger;

public class PaillierKeyPairContainer {

    private PaillierPublicKey paillierPublicKey;
    private PaillierPrivateKey paillierPrivateKey;

    public PaillierKeyPairContainer(PaillierPublicKey paillierPublicKey,
                                    PaillierPrivateKey paillierPrivateKey) {
        this.paillierPublicKey = paillierPublicKey;
        this.paillierPrivateKey = paillierPrivateKey;
    }

    public PaillierPublicKey getPaillierPublicKey() {
        return paillierPublicKey;
    }

    public PaillierPrivateKey getPaillierPrivateKey() {
        return paillierPrivateKey;
    }

    public BigInteger encrypt(BigInteger input) {
        return this.paillierPublicKey.encrypt(input);
    }

    public BigInteger decrypt(BigInteger cipher) {
        return this.paillierPrivateKey.decrypt(cipher);
    }

    //cool stuff
    public BigInteger homomorphicSum(BigInteger ciphered_1, BigInteger ciphered_2) {
        return ciphered_1.multiply(ciphered_2).mod(paillierPublicKey.getnSquared());
    }

    public BigInteger homomorphicSum(BigInteger ciphered_1, Integer plain_2) {
        BigInteger ciphered_2 = this.encrypt(BigInteger.valueOf(plain_2));
        return ciphered_1.multiply(ciphered_2).mod(paillierPublicKey.getnSquared());
    }

    public BigInteger homomorphicSum(Integer plain_1, Integer plain_2) {

        BigInteger ba1 = BigInteger.valueOf(plain_1);
        BigInteger ciphered_1 = this.encrypt(ba1);

        BigInteger ba2 = BigInteger.valueOf(plain_2);
        BigInteger ciphered_2 = this.encrypt(ba2);

        return ciphered_1.multiply(ciphered_2).mod(paillierPublicKey.getnSquared());

    }

    public BigInteger sumAndDecrypt(Integer p1, Integer p2) {

        return this.decrypt(  this.homomorphicSum(p1, p2) );

    }

    public BigInteger sumAndDecrypt(BigInteger c1, BigInteger c2) {

        return this.decrypt(  this.homomorphicSum(c1, c2) );

    }

}
